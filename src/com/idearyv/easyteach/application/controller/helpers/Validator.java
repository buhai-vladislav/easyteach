package com.idearyv.easyteach.application.controller.helpers;

import java.util.regex.Pattern;

public class Validator {
	private static Pattern pattern;
	
	private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String NAME = "^([A-ZА-Я][а-яa-z]*((\\s)))+[A-ZА-Я][а-яa-z]*((\\s))+[A-ZА-Я][а-яa-z]*$";
	private static final String PASSWORD = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,15})";
	private static final String LOGIN = "^[a-zA-Z0-9._-]{3,}$";
	
	private static Pattern getPattern(ValidatorType validatorType) {
		switch (validatorType) {
		case EMAIL:
			pattern = Pattern.compile(EMAIL_PATTERN);
			break;
		case PASSWORD:
			pattern = Pattern.compile(PASSWORD);
			break;
		case LOGIN:
			pattern = Pattern.compile(LOGIN);
			break;
		case NAME:
			pattern = Pattern.compile(NAME);
			break;
		default:
			break;
		}
		return pattern;
	}
	
	public static boolean validate(final String hex, ValidatorType validatorType) {
        return getPattern(validatorType).matcher(hex).matches();
    }
}
