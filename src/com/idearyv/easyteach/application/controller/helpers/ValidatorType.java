package com.idearyv.easyteach.application.controller.helpers;

public enum ValidatorType {
	EMAIL, NAME, PASSWORD, LOGIN
}
