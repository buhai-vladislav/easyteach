package com.idearyv.easyteach.application.controller.helpers;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.person.information.testinfo.TestsInformation;
import com.idearyv.easyteach.application.serialization.SerializationHelper;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXDialog.DialogTransition;

import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class FuncsHelper {
	
	private final static int SIXTY_CONVENTIONAL_UNITS = 60;
	private final static int TEN_CONVENTIONAL_UNITS = 10;
	
    public static JFXButton createButton(String text,int xPos,int yPos,String styleProperty,
                                    int prefHeight,int prefWidth){
        JFXButton button = new JFXButton();
        button.setPrefHeight(prefHeight);
        button.setPrefWidth(prefWidth);
        button.setText(text);
        button.setStyle(styleProperty);
        return button;
    }
    
    public static void createDiloge(StackPane stackPane,String styleButtonProperty,String heading,
    		String bodyText,DialogTransition dialogTransition,String buttonText,String dialogeStyle) {
    	JFXDialogLayout content = new JFXDialogLayout();
		content.setHeading(new Text(heading));
		content.setBody(new Text(bodyText));
		JFXDialog jfxDialog = new JFXDialog(stackPane, content, dialogTransition);
		JFXButton button = FuncsHelper.createButton(buttonText, 0, 0, styleButtonProperty, 40, 100);
		jfxDialog.setLayoutX(100);
		jfxDialog.setLayoutY(100);
		jfxDialog.setStyle(dialogeStyle);
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				jfxDialog.close();
			}
		});
		content.setActions(button);
		jfxDialog.show();
    }
    
    public static ArrayList<Long> toArray(Person person, TypeOfArray typeOfArray){
    	ArrayList<Long> outputSet = new ArrayList<>();
    	Iterator<TestsInformation> iterator = person.getPersonInformation().getInformations().iterator();
    	if(typeOfArray == TypeOfArray.TIME) {
    		while(iterator.hasNext()) {
    			outputSet.add(iterator.next().getTestTime());
    		}
    	}
    	if(typeOfArray == TypeOfArray.TEST_MARK) {
    		while(iterator.hasNext()) {
    			outputSet.add((long)iterator.next().getNumberOfPassTest());
    		}
    	}	
    	return outputSet;
    }
    
    public static Person getPerson(ObservableSet<Person> set, String nickname) {
		Person person = null;
		for(Person object : set) {
			if(object.getLogin().equals(nickname)) {
				person = object;
				return person;
			}
		}
		return person;
	}
    
	public static double findValue(ArrayList<Long> set, TypeOfFindValue typeOfFindValue) {
		double value = 0;
		if (set.isEmpty() == false) {
			if (typeOfFindValue == TypeOfFindValue.AVG_VALUE) {
				for (Object number : set.toArray()) {
					value += (long) number;
				}
				value = value / set.size();
			} else {
				long tempValue = 0l;
				Iterator<Long> iterator = set.iterator();
				value = iterator.next();
				while (iterator.hasNext()) {
					tempValue = iterator.next();
					if (typeOfFindValue == TypeOfFindValue.MIN_VALUE) {
						if (value > tempValue) {
							value = tempValue;
						}
					}
					if (typeOfFindValue == TypeOfFindValue.MAX_VALUE) {
						if (value < tempValue) {
							value = tempValue;
						}
					}
				}
			}
		}
		return value;
	}
    
    public static ObservableSet<Person> saveData(ObservableSet<Person> set , String login, String testName,
    		long testTime, int numberOfPassTest , int allNumberOfTest) {
    	Iterator<Person> personIterator = set.iterator();
    	Person person = null;
    	while(personIterator.hasNext()) {
    		person = personIterator.next();
    		if(person.getLogin().equals(login)) {
    			person.getPersonInformation().getInformations().add(
    					new TestsInformation(testName, numberOfPassTest, 
    							testTime, numberOfPassTest, allNumberOfTest));
    			SerializationHelper.serialize("persons.txt", set);
    		}
    	}
     	return set;
    }
    
	public static String getTestTime(long time) {
		StringBuilder builder = new StringBuilder();
		int seconds = 0;
		int hours = 0;
		double minutes = 0.0;

		DecimalFormat format = new DecimalFormat("#.##");
		format.setRoundingMode(RoundingMode.CEILING);
		String min = null;
		minutes = (double) time / SIXTY_CONVENTIONAL_UNITS;
		Double rest = minutes - ((int) minutes);
		if (rest.equals(0.00)) {
			seconds = 0;
		} else {
			String buffer[] = format.format(rest).split(",");
			builder.append(buffer[0] + "." + buffer[1]);
			seconds = (int) (Double.parseDouble(builder.toString()) * SIXTY_CONVENTIONAL_UNITS);
			builder.delete(0, builder.length());
		}
		if ((int) minutes >= SIXTY_CONVENTIONAL_UNITS) {
			hours = (int) ((int) minutes / SIXTY_CONVENTIONAL_UNITS);
			minutes -= hours * SIXTY_CONVENTIONAL_UNITS;
			if (hours >= TEN_CONVENTIONAL_UNITS) {
					builder.append(hours + ":" + min + ":" + seconds);
			} else {
				builder.append("0" + hours + ":" + min + ":" + seconds);
			}
		} else {
			if((int)minutes <= TEN_CONVENTIONAL_UNITS) {
				min = "0" + (int)minutes;
			} else {
				min = String.valueOf((int)minutes);
			}
			builder.append("00:" + min + ":" + seconds);
		}

		return builder.toString();
	}
    
    public static long getDateDiff(Date firstDate, Date secondDate, TimeUnit timeUnit) {
	    long diffInMillies = secondDate.getTime() - firstDate.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
}
