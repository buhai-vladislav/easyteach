package com.idearyv.easyteach.application.controller;

import java.io.IOException;

import com.idearyv.easyteach.application.MainApplication;
import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class StatisticSceneController {
	private Stage stage;
	private MainApplication mainApplication;
	private String email;
	private String nickName;
	
	@FXML
	private JFXButton returnButton = new JFXButton();
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private StackPane stackPane;
	
	@FXML
	private Label avgTestTime;
	@FXML
	private Label avgTestMark;
	@FXML
	private Label minTestMark;
	@FXML
	private Label maxTestMark;
	@FXML
	private Label minTestTime;
	@FXML
	private Label maxTestTime;
	
	@FXML
    private void initialize() {
		returnButton.setGraphic(new ImageView(new Image(getClass().
				getResourceAsStream("icons/arrow_down.png"))));
		returnButton.setLayoutY(230);
		returnButton.setLayoutX(8);
		returnButton.toFront();
    }
	
	public StackPane getStackPane() {
		return stackPane;
	}

	public void setStackPane(StackPane stackPane) {
		this.stackPane = stackPane;
	}

	public AnchorPane getAnchorPane() {
		return anchorPane;
	}

	public void setAnchorPane(AnchorPane anchorPane) {
		this.anchorPane = anchorPane;
	}

	@FXML
	private void returnButton() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/ThirdStage.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			ThirdSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainApplication);
			mainApplication.getRootLayout().setCenter(anchorPane);
			sceneController.getEmailField().setText(email);
			sceneController.getNickname().setText(nickName);
			sceneController.setEmail(email);
			sceneController.setNickName(nickName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public JFXButton getReturnButton() {
		return returnButton;
	}

	public void setReturnButton(JFXButton returnButton) {
		this.returnButton = returnButton;
	}

	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	public MainApplication getMainApplication() {
		return mainApplication;
	}
	public void setMainApplication(MainApplication mainApplication) {
		this.mainApplication = mainApplication;
	}

	public Label getAvgTestTime() {
		return avgTestTime;
	}

	public Label getAvgTestMark() {
		return avgTestMark;
	}

	public Label getMinTestMark() {
		return minTestMark;
	}

	public Label getMaxTestMark() {
		return maxTestMark;
	}

	public Label getMinTestTime() {
		return minTestTime;
	}

	public Label getMaxTestTime() {
		return maxTestTime;
	}
}
