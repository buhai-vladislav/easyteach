package com.idearyv.easyteach.application.controller;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.idearyv.easyteach.application.MainApplication;
import com.idearyv.easyteach.application.controller.helpers.FuncsHelper;
import com.idearyv.easyteach.application.tests.Test;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXDialog.DialogTransition;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class TestSceneController {
	private ObservableList<Test> testList = FXCollections.observableArrayList();
	private ObservableList<Boolean> booleans = FXCollections.observableArrayList();
	private static final String styleProperty = "-fx-border-color:#10388A; -fx-text-fill:#10388A;";
	private MainApplication mainApplication;
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private StackPane stackPane;
	@FXML
	private Label info;
	@FXML
	private JFXButton backButton = new JFXButton();
	@FXML
	private JFXButton forwardButton = new JFXButton();
	@FXML
	private JFXButton returnButton = new JFXButton();
	@FXML
	private JFXTextArea testArea;
	@FXML
	private JFXCheckBox firstCheckBox;
	@FXML
	private JFXCheckBox secondCheckBox;
	@FXML
	private JFXCheckBox thirdCheckBox;
	@FXML
	private JFXCheckBox fourthCheckBox;

	private String email;
	private String nickName;
	private Date startTime;

	private boolean[] arrayCheacker;
	private int globalCounter;
	private int сounter;
	boolean flag = false;

	public TestSceneController() {
		this.firstCheckBox = new JFXCheckBox();
		this.secondCheckBox = new JFXCheckBox();
		this.thirdCheckBox = new JFXCheckBox();
		this.fourthCheckBox = new JFXCheckBox();
		this.testArea = new JFXTextArea();
		arrayCheacker = new boolean[4];
		this.globalCounter = 1;
		this.сounter = 0;
	}

	@FXML
	private void initialize() {
		forwardButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("icons/next.png"))));
		forwardButton.setText("Ответить");
		forwardButton.setStyle("-fx-font-size:20px; -fx-text-fill:white;");
		returnButton.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("icons/return_arrow.png"))));
		returnButton.setLayoutY(1);
		forwardButton.setLayoutY(3);
		forwardButton.setLayoutX(620);
	}

	@FXML
	private void backButton() {

	}

	private void resetCheckBox() {
		firstCheckBox.setSelected(false);
		secondCheckBox.setSelected(false);
		thirdCheckBox.setSelected(false);
		fourthCheckBox.setSelected(false);
	}

	@FXML
	private void forwardButton() {
		boolean temp = cheacker();
		int result = 0;
		if (temp == true) {
			if (globalCounter == testList.size()) {
				resetCheckBox();
				Date date = new Date();
				

				for (int j = 0; j < booleans.size(); j++) {
					if (booleans.get(j) == true) {
						result++;
					}
				}

				globalCounter = 10;
				JFXDialogLayout dialogLayout = new JFXDialogLayout();
				dialogLayout.setHeading(new Text("Результаты"));
				dialogLayout.setBody(new Text("Время прохождения теста: " 
						+ FuncsHelper.getTestTime(FuncsHelper.getDateDiff(startTime, date, TimeUnit.SECONDS))
						+ "\nПравильных ответов " + result + " из 10"));
				JFXDialog jfxDialog = new JFXDialog(stackPane, dialogLayout, DialogTransition.CENTER);
				JFXButton okButton = FuncsHelper.createButton("Ok", 0, 0,
						"-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
				
				FuncsHelper.saveData(mainApplication.getObservableSet(), 
						nickName, "ua", FuncsHelper.getDateDiff(startTime, date, TimeUnit.SECONDS), result, 10);
				
				okButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						FXMLLoader fxmlLoader = new FXMLLoader();
						fxmlLoader.setLocation(MainApplication.class.getResource("controller/TestsPage.fxml"));
						try {
							AnchorPane anchorPane = fxmlLoader.load();
							ChoseTestSceneController sceneController = fxmlLoader.getController();
							sceneController.setMainApplication(mainApplication);
							sceneController.setEmail(email);
							sceneController.setNickName(nickName);
							mainApplication.getRootLayout().setCenter(anchorPane);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				});
				dialogLayout.setStyle("-fx-font-size:22px;");
				dialogLayout.setActions(okButton);
				jfxDialog.show();
				return;
			} else {
				
				flag = false;
				getTestArea().setText(testList.get(globalCounter).getQuestion());
				getFirstCheckBox().setText(testList.get(globalCounter).getFirstAnswer().getAnswer());
				getSecondCheckBox().setText(testList.get(globalCounter).getSecondAnswer().getAnswer());
				getThirdCheckBox().setText(testList.get(globalCounter).getThirdAnswer().getAnswer());
				getFourthCheckBox().setText(testList.get(globalCounter).getFourthAnswer().getAnswer());
				globalCounter++;
			}
		}
		resetArrayCheacker();
		resetCheckBox();
	}

	@FXML
	private void returnArrow() {
		JFXDialogLayout dialogLayout = new JFXDialogLayout();
		dialogLayout.setHeading(new Text("Завершение тестирования"));
		dialogLayout.setBody(new Text("Вы действительно хотите завершить работу с тестом?"));
		JFXDialog jfxDialog = new JFXDialog(stackPane, dialogLayout, DialogTransition.LEFT);
		JFXButton okButton = FuncsHelper.createButton("Ok", 0, 0, "-fx-text-fill:#10388A; -fx-border-color:#10388A;",
				40, 100);
		JFXButton cancelButton = FuncsHelper.createButton("Cancel", 0, 0,
				"-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				jfxDialog.close();
			}
		});
		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				fxmlLoader.setLocation(MainApplication.class.getResource("controller/TestsPage.fxml"));
				try {
					AnchorPane anchorPane = fxmlLoader.load();
					ChoseTestSceneController sceneController = fxmlLoader.getController();
					sceneController.setMainApplication(mainApplication);
					sceneController.setEmail(email);
					sceneController.setNickName(nickName);
					mainApplication.getRootLayout().setCenter(anchorPane);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		dialogLayout.setActions(okButton, cancelButton);
		jfxDialog.show();
	}

	@FXML
	private void testArea() {

	}

	private void alert() {
		FuncsHelper.createDiloge(stackPane, styleProperty, "Ошибка", "Выберите один ответ\n", DialogTransition.LEFT,
				"Ok", "-fx-border-color:red; -fx-font-size:16px;");
		resetArrayCheacker();
		resetCheckBox();
	}

	private void checkBoxController(int index) {
		arrayCheacker[index] = true;
		if (flag == true) {
			alert();
			flag = false;
			return;
		}
		flag = true;
	}

	@FXML
	private void firstCheckBoxController() {
		checkBoxController(0);
	}

	@FXML
	private void secondCheckBoxController() {
		checkBoxController(1);
	}

	@FXML
	private void thirdCheckBoxController() {
		checkBoxController(2);
	}

	@FXML
	private void fourthCheckBoxController() {
		checkBoxController(3);
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	private void resetArrayCheacker() {
		for (int i = 0; i < arrayCheacker.length; i++) {
			arrayCheacker[i] = false;
		}
	}

	private int getArrayIndex() {
		for (int i = 0; i < arrayCheacker.length; i++) {
			if (arrayCheacker[i] == true)
				return i;
		}
		return 0;
	}
	
	private boolean isEmpty() {
		for(int i = 0; i < arrayCheacker.length; i++) {
			if(arrayCheacker[i] == true) {
				return false;
			}
		}
		return true;
	}

	private boolean cheacker() {
		boolean localFlag = false;
		if (isEmpty() == false) {
			while (localFlag == false) {
				switch (getArrayIndex()) {
				case 0:
					if (arrayCheacker[getArrayIndex()] == testList.get(сounter).getFirstAnswer().isCorrect()) {
						booleans.add(true);
					} else {
						booleans.add(false);
					}
					сounter++;
					return true;
				case 1:
					if (arrayCheacker[getArrayIndex()] == testList.get(сounter).getSecondAnswer().isCorrect()) {
						booleans.add(true);
					} else {
						booleans.add(false);
					}
					сounter++;
					return true;
				case 2:
					if (arrayCheacker[getArrayIndex()] == testList.get(сounter).getThirdAnswer().isCorrect()) {
						booleans.add(true);
					} else {
						booleans.add(false);
					}
					сounter++;
					return true;
				case 3:
					if (arrayCheacker[getArrayIndex()] == testList.get(сounter).getFourthAnswer().isCorrect()) {
						booleans.add(true);
					} else {
						booleans.add(false);
					}
					сounter++;
					return true;
				default:
					return false;
				}
			}
		}

		return false;
	}

	public JFXTextArea getTestArea() {
		return testArea;
	}

	public void setTestArea(JFXTextArea testArea) {
		this.testArea = testArea;
	}

	public JFXCheckBox getFirstCheckBox() {
		return firstCheckBox;
	}

	public void setFirstCheckBox(JFXCheckBox firstCheckBox) {
		this.firstCheckBox = firstCheckBox;
	}

	public JFXCheckBox getSecondCheckBox() {
		return secondCheckBox;
	}

	public void setSecondCheckBox(JFXCheckBox secondCheckBox) {
		this.secondCheckBox = secondCheckBox;
	}

	public JFXCheckBox getThirdCheckBox() {
		return thirdCheckBox;
	}

	public void setThirdCheckBox(JFXCheckBox thirdCheckBox) {
		this.thirdCheckBox = thirdCheckBox;
	}

	public JFXCheckBox getFourthCheckBox() {
		return fourthCheckBox;
	}

	public void setFourthCheckBox(JFXCheckBox fourthCheckBox) {
		this.fourthCheckBox = fourthCheckBox;
	}

	public ObservableList<Boolean> getBooleans() {
		return booleans;
	}

	public void setBooleans(ObservableList<Boolean> booleans) {
		this.booleans = booleans;
	}

	public ObservableList<Test> getTestList() {
		return testList;
	}

	public void setTestList(ObservableList<Test> testList) {
		this.testList = testList;
	}

	public MainApplication getMainApplication() {
		return mainApplication;
	}

	public void setMainApplication(MainApplication mainApplication) {
		this.mainApplication = mainApplication;
	}

	public Label getInfo() {
		return info;
	}

	public void setInfo(Label info) {
		this.info = info;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
}
