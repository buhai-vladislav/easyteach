package com.idearyv.easyteach.application.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import com.idearyv.easyteach.application.MainApplication;
import com.idearyv.easyteach.application.serialization.SerializationHelper;
import com.idearyv.easyteach.application.tests.Test;
import com.jfoenix.controls.JFXButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class ChoseTestSceneController {
	
	private MainApplication mainApplication;

	@FXML
	private AnchorPane anchorPane;

	@FXML
	private JFXButton firstButton = new JFXButton();
	@FXML
	private JFXButton secondButton = new JFXButton();
	@FXML
	private JFXButton returnButton = new JFXButton();
	
	private String email;
	private String nickName;

	@FXML
    private void initialize() {
		firstButton.setGraphic(new ImageView(new Image(getClass().
				getResourceAsStream("icons/FirstButton.png"))));
		secondButton.setGraphic(new ImageView(new Image(getClass().
				getResourceAsStream("icons/math.png"))));
		returnButton.setGraphic(new ImageView(new Image(getClass().
				getResourceAsStream("icons/arrow_down.png"))));
		returnButton.setLayoutY(230);
    }
	
	@FXML
	private void returnButton() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/ThirdStage.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			ThirdSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainApplication);
			mainApplication.getRootLayout().setCenter(anchorPane);
			sceneController.getEmailField().setText(email);
			sceneController.getNickname().setText(nickName);
			sceneController.setEmail(email);
			sceneController.setNickName(nickName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void firstButton() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/TestViewWindow.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			TestSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainApplication);
			sceneController.setEmail(email);
			sceneController.setNickName(nickName);
			ObservableList<Test> tests = FXCollections.observableArrayList();
			ObservableSet<Test> object = SerializationHelper.deSerialize("tests.txt", TypeOfClass.TESTS);
			Iterator<Test> iterator = object.iterator();
			while(iterator.hasNext()) {
				tests.add(iterator.next());
			}
			
			Date date = new Date();	
		
			sceneController.setTestList(tests);
			sceneController.setStartTime(date);
			mainApplication.getRootLayout().setCenter(anchorPane);
			sceneController.getTestArea().setText(tests.get(0).getQuestion());
			sceneController.getFirstCheckBox().setText(tests.get(0).getFirstAnswer().getAnswer());
			sceneController.getSecondCheckBox().setText(tests.get(0).getSecondAnswer().getAnswer());
			sceneController.getThirdCheckBox().setText(tests.get(0).getThirdAnswer().getAnswer());
			sceneController.getFourthCheckBox().setText(tests.get(0).getFourthAnswer().getAnswer());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public MainApplication getMainApplication() {
		return mainApplication;
	}


	public void setMainApplication(MainApplication mainApplication) {
		this.mainApplication = mainApplication;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
}
