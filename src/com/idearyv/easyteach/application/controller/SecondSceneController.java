package com.idearyv.easyteach.application.controller;

import java.io.IOException;
import java.util.Iterator;

import com.idearyv.easyteach.application.MainApplication;
import com.idearyv.easyteach.application.controller.helpers.FuncsHelper;
import com.idearyv.easyteach.application.person.Person;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXDialog.DialogTransition;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SecondSceneController {
	@FXML
	private StackPane stackPane;
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private JFXPasswordField passwordField;
	@FXML
	private JFXTextField textField;
	@FXML
	private JFXButton button;
	@FXML
	private Hyperlink hyperlink = new Hyperlink();

	private MainApplication mainapp;

	private Stage registrationStage = new Stage();

	private static final String styleProperty = "-fx-border-color:#10388A; -fx-text-fill:#10388A;";;

	@FXML
	private void initialize() {
	}

	public boolean showRegistration() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApplication.class.getResource("controller/RegistrationWindow.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			registrationStage.setTitle("Регистрация");
			registrationStage.initModality(Modality.WINDOW_MODAL);
			registrationStage.initOwner(mainapp.getStage());
			Scene scene = new Scene(page);
			registrationStage.setScene(scene);

			registrationStage.getIcons().add(new Image("file:resources/images/logo.png"));

			RegistrationWindowController controller = loader.getController();
			controller.setStage(registrationStage);
			controller.setMainApplication(mainapp);

			registrationStage.showAndWait();
			return controller.isOkClicked();
		} catch (IOException exception) {
			exception.printStackTrace();
			return false;
		}
	}

	@FXML
	private void hyperlink() {
		showRegistration();
	}

	public Stage getRegistrationStage() {
		return registrationStage;
	}

	public void setRegistrationStage(Stage registrationStage) {
		this.registrationStage = registrationStage;
	}

	public String getTextFieldInfo() {
		return textField.getText();
	}

	public String getTextPasswordField() {
		return passwordField.getText();
	}

	@FXML
	public void buttonFunc() {
		Iterator<Person> personIterator = mainapp.getObservableSet().iterator();
		Person person = null;
		person = personIterator.next();
		boolean flag = false;
		while (flag == false) {
			if (person.getLogin().equals(textField.getText())) {
				if (person.getPassword().equals(passwordField.getText())) {
					flag = true;
					FXMLLoader fxmlLoader = new FXMLLoader();
					fxmlLoader.setLocation(MainApplication.class.getResource("controller/ThirdStage.fxml"));
					try {
						AnchorPane anchorPane = fxmlLoader.load();
						ThirdSceneController sceneController = fxmlLoader.getController();
						sceneController.setMainApplication(mainapp);
						sceneController.getEmailField().setText(person.getEmail());
						sceneController.getNickname().setText(person.getLogin());
						sceneController.setEmail(person.getEmail());
						sceneController.setNickName(person.getLogin());
						mainapp.getRootLayout().setCenter(anchorPane);
					} catch (IOException e) {
						e.printStackTrace();
					}
					return;
				} else {
					FuncsHelper.createDiloge(stackPane, styleProperty, "Ошибка", "Неверный пароль\n",
							DialogTransition.LEFT, "Ok", "-fx-border-color:red; -fx-font-size:16px;");
					passwordField.clear();
					break;
				}
			} else {
				if (personIterator.hasNext() != true) {
					FuncsHelper.createDiloge(stackPane, styleProperty, "Ошибка", "Неверный логин\n",
							DialogTransition.LEFT, "Ok", "-fx-border-color:red; -fx-font-size:16px;");
					textField.clear();
					passwordField.clear();
					break;
				} else {
					person = personIterator.next();
				}
			}
		}
	}

	public void setMainApplication(MainApplication mainApplication) {
		this.mainapp = mainApplication;
	}
}
