package com.idearyv.easyteach.application.controller;

import java.util.ArrayList;

import com.idearyv.easyteach.application.MainApplication;
import com.idearyv.easyteach.application.controller.helpers.FuncsHelper;
import com.idearyv.easyteach.application.controller.helpers.Validator;
import com.idearyv.easyteach.application.controller.helpers.ValidatorType;
import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.person.information.PersonInformation;
import com.idearyv.easyteach.application.serialization.SerializationHelper;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXDialog.DialogTransition;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class RegistrationWindowController {
	private Stage stage;
	private MainApplication mainApplication;
	private static final String styleProperty = "-fx-border-color:#10388A; -fx-text-fill:#10388A;";
	@FXML
	private AnchorPane anchorPane;
	@FXML
	private StackPane stackPane;
	@FXML
	private JFXTextField loginField;
	@FXML
	private JFXTextField passwordFiled;
	@FXML
	private JFXTextField emailField;
	@FXML
	private JFXTextField infoField;
	@FXML
	private JFXButton enterButton;
	
	private boolean flag = false;
	private String message;
	
	private boolean okClicked = false;
	
	public boolean isOkClicked() {
		return okClicked;
	}

	public void setOkClicked(boolean okClicked) {
		this.okClicked = okClicked;
	}

	@FXML
	private void initialize() {
	}
	
	@FXML
	private void enterButton() {
		validate();
		while (flag == true) {
			if (flag == true) {
				FuncsHelper.createDiloge(stackPane, styleProperty, "Ошибка", message, DialogTransition.LEFT, "Ok",
						"-fx-border-color:red; -fx-font-size:16px;");
				flag = false;
				break;
			} else {
				String[] buffer = infoField.getText().split(" ");
				Person person = new Person(loginField.getText(), emailField.getText(), passwordFiled.getText(),
						new PersonInformation(buffer[0], buffer[1], buffer[2], "", new ArrayList<>()));
				mainApplication.getObservableSet().add(person);
				SerializationHelper.serialize("persons.txt", mainApplication.getObservableSet());
				okClicked = true;
				stage.close();
				break;
			}
		}
	}
	
	private void validate() {
		if(Validator.validate(infoField.getText(), ValidatorType.NAME) == false){
			message = "Введено некорректное ФИО. \nФормат ФИО должен иметь вид - Хххх Хххх Хххх";
			flag = true;
			infoField.clear();
			return;
		}
		if(Validator.validate(loginField.getText(), ValidatorType.LOGIN) == false){
			message = "Введен некорректный логин. \nФормат логина должен иметь вид - Хххх";
			flag = true;
			loginField.clear();
			return;
		}
		if(Validator.validate(passwordFiled.getText(), ValidatorType.PASSWORD) == false){
			message = "Введен некорректный формат пароля. \nФормат пароля должен иметь вид - Хххххх(от 6 до 15 символов)";
			flag = true;
			passwordFiled.clear();
			return;
		}
		if(Validator.validate(emailField.getText(), ValidatorType.EMAIL) == false){
			message = "Введен некорректный формат почты. \nФормат почты должен иметь вид - хххххх@xxxx.xxx";
			flag = true;
			emailField.clear();
			return;
		}
	}
	
	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	public MainApplication getMainApplication() {
		return mainApplication;
	}

	public void setMainApplication(MainApplication mainApplication) {
		this.mainApplication = mainApplication;
	}
	
	public JFXTextField getLoginField() {
		return loginField;
	}
	public void setLoginField(JFXTextField loginField) {
		this.loginField = loginField;
	}
	public JFXTextField getPasswordFiled() {
		return passwordFiled;
	}
	public void setPasswordFiled(JFXTextField passwordFiled) {
		this.passwordFiled = passwordFiled;
	}
	public JFXTextField getEmailField() {
		return emailField;
	}
	public void setEmailField(JFXTextField emailField) {
		this.emailField = emailField;
	}
	public JFXTextField getInfoField() {
		return infoField;
	}
	public void setInfoField(JFXTextField infoField) {
		this.infoField = infoField;
	}
}
