package com.idearyv.easyteach.application.controller;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Iterator;

import com.idearyv.easyteach.application.MainApplication;
import com.idearyv.easyteach.application.controller.helpers.DoughnutChart;
import com.idearyv.easyteach.application.controller.helpers.FuncsHelper;
import com.idearyv.easyteach.application.controller.helpers.TypeOfArray;
import com.idearyv.easyteach.application.controller.helpers.TypeOfFindValue;
import com.idearyv.easyteach.application.person.information.testinfo.TestsInformation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialog.DialogTransition;
import com.jfoenix.controls.JFXDialogLayout;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class ThirdSceneController {
	private MainApplication mainApplication;
	@FXML
	private StackPane stackPane;
	@FXML
	private Label nickname = new Label();
	public Label getNickname() {
		return nickname;
	}

	public void setNickname(Label nickname) {
		this.nickname = nickname;
	}

	public Label getEmailField() {
		return emailField;
	}

	public void setEmailField(Label emailField) {
		this.emailField = emailField;
	}

	@FXML
	private Label emailField = new Label();
	@FXML
	private JFXButton aboutUsButton;
	@FXML
	private JFXButton statisticsButton;
	@FXML
	private JFXButton exit;
	@FXML
	private JFXButton testButton;
	@FXML
	private JFXButton returnButton = new JFXButton();
	
	private String email;
	private String nickName;
	
	
	@FXML
    private void initialize() {
		returnButton.setGraphic(new ImageView(new Image(getClass().
				getResourceAsStream("icons/arrowLeft.png"))));
		returnButton.setLayoutY(300);
    }
	
	@FXML
	private void returnButton() {
		JFXDialogLayout dialogLayout = new JFXDialogLayout();
		dialogLayout.setHeading(new Text("Выход с аккаунта"));
		dialogLayout.setBody(new Text("Вы действительно хотите выйти с аккаунта?"));
		JFXDialog jfxDialog = new JFXDialog(stackPane, dialogLayout, DialogTransition.CENTER);
		JFXButton okButton = FuncsHelper.createButton("Ok", 0, 0, "-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
		JFXButton cancelButton = FuncsHelper.createButton("Cancel", 0, 0, "-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
		        fxmlLoader.setLocation(MainApplication.class.getResource("controller/SecondScene.fxml"));
		        try {
		        	AnchorPane anchorPane = fxmlLoader.load();
		        	SecondSceneController sceneController = fxmlLoader.getController();
		        	sceneController.setMainApplication(mainApplication);
		        	mainApplication.getRootLayout().setCenter(anchorPane);
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
			}
			
		});
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				jfxDialog.close();
			}
		});
		dialogLayout.setActions(okButton,cancelButton);
		jfxDialog.show();
	}
	
	@FXML
	private void testsButtonFunc() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/TestsPage.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			ChoseTestSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainApplication);
			sceneController.setEmail(email);
			sceneController.setNickName(nickName);
			mainApplication.getRootLayout().setCenter(anchorPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private int getCorrectAnswers() {
		int correctAnswers = 0;
		Iterator<TestsInformation> iterator = FuncsHelper.getPerson(mainApplication.getObservableSet(),nickName).getPersonInformation().getInformations().iterator();
		while(iterator.hasNext()) {
			correctAnswers += iterator.next().getNumberOfPassTest();
		}
		return correctAnswers;
	}
	
	private int getIncorrectAnswers() {
		int incorrectAnswers = 0;
		Iterator<TestsInformation> iterator = FuncsHelper.getPerson(mainApplication.getObservableSet(),nickName).getPersonInformation().getInformations().iterator();
		while(iterator.hasNext()) {
			incorrectAnswers += iterator.next().getAllNumberOfTest();
		}
		return incorrectAnswers -= getCorrectAnswers();
	}
	
	private ObservableList<PieChart.Data> createData(){
		return FXCollections.observableArrayList(new PieChart.Data("Правильных: " + getCorrectAnswers(), getCorrectAnswers()),
				new PieChart.Data("Неправильных: " + getIncorrectAnswers() ,getIncorrectAnswers()));
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	@FXML
	private void aboutUsButtonFunc() {
		FuncsHelper.createDiloge(stackPane, "-fx-text-fill:#10388A; -fx-border-color:#10388A;", "О разработчике", "Разработал студент НТУ-"+"ХПИ" + 
				"\nкафедры ОТП, группы КИТ-26а Бугай В.С.", 
				DialogTransition.LEFT, "Ok",
				"-fx-border-color:#10388A; -fx-font-size:16px;");
	}
	
	@FXML
	private void statisticsButtonFunc() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/StatisticPage.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			ObservableList<PieChart.Data> pieChartData = createData();
	        DoughnutChart chart = new DoughnutChart(pieChartData);
	        chart.setLabelsVisible(Boolean.FALSE);
			StatisticSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainApplication);
			sceneController.setEmail(email);
			sceneController.setNickName(nickName);
			mainApplication.getRootLayout().setCenter(anchorPane);
			sceneController.getStackPane().getChildren().add(chart);
			setStatistics(sceneController);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setStatistics(StatisticSceneController sceneController) {
		DecimalFormat format = new DecimalFormat("#.##");
		format.setRoundingMode(RoundingMode.CEILING);
		sceneController.getAvgTestTime().setText(sceneController.getAvgTestTime().getText() + ": " 
				+ FuncsHelper.getTestTime((long) FuncsHelper.findValue(FuncsHelper.toArray(
						FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), TypeOfArray.TIME), TypeOfFindValue.AVG_VALUE)));
		sceneController.getAvgTestMark().setText(sceneController.getAvgTestMark().getText() + ": "
				+ format.format(FuncsHelper.findValue(FuncsHelper.toArray(FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), 
						TypeOfArray.TEST_MARK), TypeOfFindValue.AVG_VALUE)));
		sceneController.getMinTestMark().setText(sceneController.getMinTestMark().getText() + ": "
				+ FuncsHelper.findValue(FuncsHelper.toArray(FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), 
						TypeOfArray.TEST_MARK), TypeOfFindValue.MIN_VALUE));
		sceneController.getMaxTestMark().setText(sceneController.getMaxTestMark().getText() + ": "
				+ FuncsHelper.findValue(FuncsHelper.toArray(FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), 
						TypeOfArray.TEST_MARK), TypeOfFindValue.MAX_VALUE));
		sceneController.getMinTestTime().setText(sceneController.getMinTestTime().getText() + ": " 
				+ FuncsHelper.getTestTime((long) FuncsHelper.findValue(FuncsHelper.toArray(
						FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), TypeOfArray.TIME), TypeOfFindValue.MIN_VALUE)));
		sceneController.getMaxTestTime().setText(sceneController.getMaxTestTime().getText() + ": " 
				+ FuncsHelper.getTestTime((long) FuncsHelper.findValue(FuncsHelper.toArray(
						FuncsHelper.getPerson(mainApplication.getObservableSet(), nickName), TypeOfArray.TIME), TypeOfFindValue.MAX_VALUE)));
	
	}
	
	
	@FXML
	private void exitButtonFunc() {
		JFXDialogLayout dialogLayout = new JFXDialogLayout();
		dialogLayout.setHeading(new Text("Завершение работы"));
		dialogLayout.setBody(new Text("Вы действительно хотите завершить работу программы?"));
		JFXDialog jfxDialog = new JFXDialog(stackPane, dialogLayout, DialogTransition.CENTER);
		JFXButton okButton = FuncsHelper.createButton("Ok", 0, 0, "-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
		JFXButton cancelButton = FuncsHelper.createButton("Cancel", 0, 0, "-fx-text-fill:#10388A; -fx-border-color:#10388A;", 40, 100);
		okButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
			
		});
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				jfxDialog.close();
			}
		});
		dialogLayout.setActions(okButton,cancelButton);
		jfxDialog.show();
		
	}
	
	public void setMainApplication(MainApplication mainApplication) {
		this.mainApplication = mainApplication;
	}
	
	public MainApplication getMainApplication() {
		return this.mainApplication;
	}
	
}
