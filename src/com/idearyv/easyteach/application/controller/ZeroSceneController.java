package com.idearyv.easyteach.application.controller;

import java.io.IOException;

import com.idearyv.easyteach.application.MainApplication;
import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

public class ZeroSceneController {
	@FXML
	private StackPane stackPane;
	
	@FXML
	private AnchorPane anchorPane;

    private MainApplication mainapp;
    
    @FXML
    private void initialize() {
    }
    
    public void setMainApplication(MainApplication mainApplication) {
    	this.mainapp = mainApplication;
    }

    @FXML
    private JFXButton button;

    @FXML
	public void func() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource("controller/SecondScene.fxml"));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			SecondSceneController sceneController = fxmlLoader.getController();
			sceneController.setMainApplication(mainapp);
			mainapp.getRootLayout().setCenter(anchorPane);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
