package com.idearyv.easyteach.application.tests;

import com.idearyv.easyteach.application.tests.answers.Answer;

public class Test {
	private String question;
	private Answer firstAnswer;
	private Answer secondAnswer;
	private Answer thirdAnswer;
	private Answer fourthAnswer;
	
	public Test() {
		this.question = "";
		this.firstAnswer = new Answer();
		this.secondAnswer = new Answer();
		this.thirdAnswer = new Answer();
		this.fourthAnswer = new Answer();
	}
	
	public Test(String question, Answer firstAnswer, Answer secondAnswer, Answer thirdAnswer, Answer fourthAnswer) {
		this.question = question;
		this.firstAnswer = firstAnswer;
		this.secondAnswer = secondAnswer;
		this.thirdAnswer = thirdAnswer;
		this.fourthAnswer = fourthAnswer;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Answer getFirstAnswer() {
		return firstAnswer;
	}
	public void setFirstAnswer(Answer firstAnswer) {
		this.firstAnswer = firstAnswer;
	}
	public Answer getSecondAnswer() {
		return secondAnswer;
	}
	public void setSecondAnswer(Answer secondAnswer) {
		this.secondAnswer = secondAnswer;
	}
	public Answer getThirdAnswer() {
		return thirdAnswer;
	}
	public void setThirdAnswer(Answer thirdAnswer) {
		this.thirdAnswer = thirdAnswer;
	}
	public Answer getFourthAnswer() {
		return fourthAnswer;
	}
	public void setFourthAnswer(Answer fourthAnswer) {
		this.fourthAnswer = fourthAnswer;
	}
	
	
}
