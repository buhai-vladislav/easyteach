package com.idearyv.easyteach.application.tests.answers;

public class Answer {
	private String answer;
	private boolean isCorrect;
	
	public Answer() {
		this.answer = "";
		this.isCorrect = false;
	}
	
	public Answer(String answer, boolean isCorrect) {
		this.answer = answer;
		this.isCorrect = isCorrect;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public boolean isCorrect() {
		return isCorrect;
	}
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	@Override
	public String toString() {
		return answer + "," + isCorrect;
	}
	
	
	
}
