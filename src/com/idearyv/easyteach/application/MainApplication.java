package com.idearyv.easyteach.application;

import javafx.application.Application;
import javafx.collections.ObservableSet;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

import com.idearyv.easyteach.application.controller.ZeroSceneController;
import com.idearyv.easyteach.application.controller.TypeOfClass;
import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.serialization.SerializationHelper;

public class MainApplication extends Application {

	private Stage stage;
	private BorderPane rootLayout;

	private ObservableSet<Person> observableSet;

	public ObservableSet<Person> getObservableSet() {
		return observableSet;
	}

	public void setObservableList(ObservableSet<Person> observableList) {
		this.observableSet = observableList;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Stage getStage() {
		return this.stage;
	}

	public BorderPane getRootLayout() {
		return rootLayout;
	}

	public void setRootLayout(BorderPane rootLayout) {
		this.rootLayout = rootLayout;
	}
	
	public static void main(String[] args) throws Throwable {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.stage = primaryStage;
		this.stage.setTitle("EasyTeach");

		initRootLayout();
		showNextStage("controller/ZeroStage.fxml");
	}

	private void initRootLayout() {
		observableSet = SerializationHelper.deSerialize("persons.txt",TypeOfClass.PERSON);
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(MainApplication.class.getResource("controller/RootStage.fxml"));
			rootLayout = fxmlLoader.load();

			Scene scene = new Scene(rootLayout);
			stage.setScene(scene);
			stage.getIcons().add(new Image("file:resources/images/logo.png"));
			stage.resizableProperty().setValue(Boolean.FALSE);
			stage.show();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	public void showNextStage(String stageName) {
		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setLocation(MainApplication.class.getResource(stageName));
		try {
			AnchorPane anchorPane = fxmlLoader.load();
			ZeroSceneController buttonController = fxmlLoader.getController();
			buttonController.setMainApplication(this);
			rootLayout.setCenter(anchorPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
