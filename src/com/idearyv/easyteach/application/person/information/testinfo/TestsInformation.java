package com.idearyv.easyteach.application.person.information.testinfo;

public class TestsInformation {
	
	private String testName;
	private double testMark;
	private long testTime;
	private int numberOfPassTest;
	private int allNumberOfTest;
	
	public TestsInformation() {
		this.testName = "";
		this.testMark = 0.0;
		this.testTime = 0;
		this.numberOfPassTest = 0;
		this.allNumberOfTest = 0;
	};
	
	public TestsInformation(String testName, double testMark, 
			long testTime, int numberOfPassTest, int allNumberOfTest) {
		this.testName = testName;
		this.testMark = testMark;
		this.testTime = testTime;
		this.numberOfPassTest = numberOfPassTest;
		this.allNumberOfTest = allNumberOfTest;
	}
	
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public double getTestMark() {
		return testMark;
	}
	public void setTestMark(double testMark) {
		this.testMark = testMark;
	}
	public long getTestTime() {
		return testTime;
	}
	public void setTestTime(long testTime) {
		this.testTime = testTime;
	}
	public int getNumberOfPassTest() {
		return numberOfPassTest;
	}
	public void setNumberOfPassTest(int numberOfPassTest) {
		this.numberOfPassTest = numberOfPassTest;
	}
	public int getAllNumberOfTest() {
		return allNumberOfTest;
	}
	public void setAllNumberOfTest(int allNumberOfTest) {
		this.allNumberOfTest = allNumberOfTest;
	}

	@Override
	public String toString() {
		return testName + "," + testMark + "," + testTime + "," + 
				numberOfPassTest + "," + allNumberOfTest + " ";
	}
	
	
}
