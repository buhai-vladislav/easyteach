package com.idearyv.easyteach.application.person.information;

import java.util.ArrayList;
import java.util.List;

import com.idearyv.easyteach.application.person.information.testinfo.TestsInformation;

public class PersonInformation {
	private String name;
	private String surname;
	private String secondName;
	private String institution;
	private List<TestsInformation> informations;

	public PersonInformation() {
		this.name = "";
		this.surname = "";
		this.secondName = "";
		this.institution = "";
		this.informations = new ArrayList<TestsInformation>();
	};
	
	public PersonInformation(String name, String surname, 
			String secondName, String institution,
			List<TestsInformation> informations) {
		this.name = name;
		this.surname = surname;
		this.secondName = secondName;
		this.institution = institution;
		this.informations = informations;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public List<TestsInformation> getInformations() {
		return informations;
	}

	public void setInformations(List<TestsInformation> informations) {
		this.informations = informations;
	}
}
