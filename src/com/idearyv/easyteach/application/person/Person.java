package com.idearyv.easyteach.application.person;

import com.idearyv.easyteach.application.person.information.PersonInformation;

public class Person {
	private String login;
	private String email;
	private String password;
	private PersonInformation personInformation;

	public Person() {
		this.login = "";
		this.email = "";
		this.password = "";
		this.personInformation = new PersonInformation();
	};

	public Person(String login, String email, String password, PersonInformation personInformation) {
		this.login = login;
		this.email = email;
		this.password = password;
		this.personInformation = personInformation;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PersonInformation getPersonInformation() {
		return personInformation;
	}

	public void setPersonInformation(PersonInformation personInformation) {
		this.personInformation = personInformation;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this)
			return true;

		if (object == null || object.getClass() != this.getClass())
			return false;

		Person person = (Person) object;
		return login == person.login && (email == person.email 
				|| email != null && email.equals(person.email));

	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + login.length();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

}
