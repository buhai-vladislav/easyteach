package com.idearyv.easyteach.application.serialization;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.person.information.PersonInformation;
import com.idearyv.easyteach.application.person.information.testinfo.TestsInformation;


public class CustomSerialization implements JsonSerializer<Person>, JsonDeserializer<Person> {

	@Override
	public JsonElement serialize(Person person, Type type, JsonSerializationContext context) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("login", person.getLogin());
		jsonObject.addProperty("email", person.getEmail());
		jsonObject.addProperty("password", person.getPassword());
		jsonObject.addProperty("name", person.getPersonInformation().getName());
		jsonObject.addProperty("surname", person.getPersonInformation().getSurname());
		jsonObject.addProperty("secondName", person.getPersonInformation().getSecondName());
		jsonObject.addProperty("institution", person.getPersonInformation().getInstitution());
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < person.getPersonInformation().getInformations().size(); i++) {
			builder.append(person.getPersonInformation().getInformations().get(i).toString());
		}
		jsonObject.addProperty("informations", builder.toString());
		return jsonObject;
	}

	@Override
	public Person deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject jsonObject = jsonElement.getAsJsonObject();
		String login = new String(jsonObject.get("login").getAsString());
		String email = new String(jsonObject.get("email").getAsString());
		String password = new String(jsonObject.get("password").getAsString());
		String name = new String(jsonObject.get("name").getAsString());
		String surname = new String(jsonObject.get("surname").getAsString());
		String secondName = new String(jsonObject.get("secondName").getAsString());
		String institution = new String(jsonObject.get("institution").getAsString());
		String[] elem = jsonObject.get("informations").getAsString().split(" ");
		ArrayList<TestsInformation> list = new ArrayList<>();
		String[] buffer;
		for (int i = 0; i < elem.length; i++) {
			buffer = elem[i].split(",");
			if (buffer[0] == null | buffer[0].equals("")) {
				list.add(new TestsInformation());
			} else {
				list.add(new TestsInformation(buffer[0], Double.parseDouble(buffer[1]), Integer.parseInt(buffer[2]),
						Integer.parseInt(buffer[3]), Integer.parseInt(buffer[4])));
			}
		}

		return new Person(login, email, password, new PersonInformation(name, surname, secondName, institution, list));
	}

}
