package com.idearyv.easyteach.application.serialization;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.idearyv.easyteach.application.controller.TypeOfClass;
import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.tests.Test;

import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

public class SerializationHelper {
	
	public static Gson getGson() {
		return new GsonBuilder().registerTypeAdapter(
				Person.class, new CustomSerialization()).create();
	}
	
	private static <T> ArrayList<String> toArray(ObservableSet<T> set){
		ArrayList<String> outPutList = new ArrayList<>();
		Gson gson = getGson();
		for(T object : set) {
			outPutList.add(gson.toJson(object));
		}	
		return outPutList;
	}
	
	@SuppressWarnings("unchecked")
	private static <T> ObservableSet<T> fromArray(ArrayList<String> list, TypeOfClass typeOfClass) {
		ObservableSet<T> outPutList = FXCollections.observableSet();
		Gson gson = getGson();
		Iterator<String> iterator = list.iterator();
		if (typeOfClass == TypeOfClass.PERSON) {
			while (iterator.hasNext()) {
				outPutList.add((T) gson.fromJson(iterator.next(), Person.class));
			}
		} else {
			while (iterator.hasNext()) {
				outPutList.add((T) gson.fromJson(iterator.next(), Test.class));
			}
		}
		return outPutList;
	}
	
	public static <T> void serialize(String fileName , ObservableSet<T> set) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
			oos.writeObject(toArray(set));
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> ObservableSet<T> deSerialize(String fileName, TypeOfClass typeOfClass){
		ArrayList<String> list = null;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
			list = (ArrayList<String>) ois.readObject();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		} finally {
			if(list == null) {
				list = new ArrayList<>();
			}
		}
		return fromArray(list, typeOfClass);
	}
}
