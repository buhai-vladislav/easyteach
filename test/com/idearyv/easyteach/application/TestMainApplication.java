package com.idearyv.easyteach.application;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.person.information.PersonInformation;

import javafx.collections.FXCollections;

class TestMainApplication {

	@Test
	void test() {
		MainApplication mainApplication = new MainApplication();
		String login = "Vlad";
		mainApplication.setObservableList(FXCollections.observableSet());
		mainApplication.getObservableSet().add(new Person(login, "qwerty@mail.com", "qwerty", new PersonInformation()));
		Iterator<Person> iterator = mainApplication.getObservableSet().iterator();
		Person person = iterator.next();
		assertAll("MainApplication",
				() -> assertNotNull(mainApplication,"MainApplicationNotNull"),
				() -> assertEquals(login, person.getLogin(), "Login"),
				() -> assertNotNull(person.getPersonInformation()),
				() -> assertNotNull(person.getPersonInformation().getInformations()));
	}

}
