package com.idearyv.easyteach.controller.helper;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.idearyv.easyteach.application.controller.TypeOfClass;
import com.idearyv.easyteach.application.controller.helpers.FuncsHelper;
import com.idearyv.easyteach.application.controller.helpers.TypeOfArray;
import com.idearyv.easyteach.application.controller.helpers.TypeOfFindValue;
import com.idearyv.easyteach.application.person.Person;
import com.idearyv.easyteach.application.person.information.PersonInformation;
import com.idearyv.easyteach.application.person.information.testinfo.TestsInformation;
import com.idearyv.easyteach.application.serialization.SerializationHelper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

class TestFuncHelper {
	
	private ObservableSet<Person> set;
	private static final String LOGIN = "Vlad";
	private static final String FILENAME = "serialize.txt";
	
	@BeforeEach
	public void init() {
		set = FXCollections.observableSet();
		set.add(new Person("wqe", "asd", "asd", new PersonInformation()));
		set.add(new Person(LOGIN, "asd", "asd", new PersonInformation()));
	}

	@Test
	void testCalculateTestTime() {
		long time = 70l;
		String str = "00:01:10";
		assertEquals(str, FuncsHelper.getTestTime(time));
	}
	
	@Test
	public void testTimeDif() {
		long sleepTime = 10l;
		Date firstDate = new Date();
		try {
			Thread.sleep(sleepTime * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Date secondDate = new Date();
		assertEquals(sleepTime, FuncsHelper.getDateDiff(firstDate, secondDate, TimeUnit.SECONDS));	
	}
	
	@Test
	public void testGetPersonFromSet() {
		Person person = FuncsHelper.getPerson(set, LOGIN);
		assertAll("Get Person",
				() -> assertNotNull(person),
				() -> assertEquals(LOGIN, person.getLogin()));
	}
	
	@Test
	public void testToArrayTime() {
		ArrayList<Long> array = FuncsHelper.toArray(
				FuncsHelper.getPerson(set, LOGIN), TypeOfArray.TIME);
		assertAll("To array",
				() -> assertNotNull(array),
				() -> assertTrue(array.isEmpty()));
	}
	
	@Test
	public void testToArrayMark() {
		ArrayList<Long> array = FuncsHelper.toArray(
				FuncsHelper.getPerson(set, LOGIN), TypeOfArray.TEST_MARK);
		assertAll("To array",
				() -> assertNotNull(array),
				() -> assertTrue(array.isEmpty()));
	}
	
	@Test
	public void testFindValueWithEmptyData() {
		assertEquals(0, FuncsHelper.findValue(FuncsHelper.toArray(
				FuncsHelper.getPerson(set, LOGIN), TypeOfArray.TIME), TypeOfFindValue.AVG_VALUE));
	}
	@Test
	public void testFindAvgValue() {
		String login = "qwertyui";
		long number = 10;
		PersonInformation information = new PersonInformation();
		information.getInformations().add(new TestsInformation("", 10, 10, 10, 10));
		set.add(new Person(login, "asd", "asd", information));
		assertEquals(number, FuncsHelper.findValue(FuncsHelper.toArray(
				FuncsHelper.getPerson(set, login), TypeOfArray.TIME), TypeOfFindValue.AVG_VALUE));
	}
	
	@Test
	public void testFindMaxValue() {
		String login = "qwertyui";
		long number = 10;
		PersonInformation information = new PersonInformation();
		information.getInformations().add(new TestsInformation("", 10, 10, 10, 10));
		set.add(new Person(login, "asd", "asd", information));
		assertEquals(number, FuncsHelper.findValue(FuncsHelper.toArray(
				FuncsHelper.getPerson(set, login), TypeOfArray.TIME), TypeOfFindValue.MAX_VALUE));
	}
	
	@Test
	public void testFindMinValue() {
		String login = "qwertyui";
		long number = 10;
		PersonInformation information = new PersonInformation();
		information.getInformations().add(new TestsInformation("", 10, 10, 10, 10));
		set.add(new Person(login, "asd", "asd", information));
		assertEquals(number, FuncsHelper.findValue(FuncsHelper.toArray(
				FuncsHelper.getPerson(set, login), TypeOfArray.TIME), TypeOfFindValue.MIN_VALUE));
	}
	
	@Test
	public void testGetGson() {
		assertEquals(Gson.class, SerializationHelper.getGson().getClass());
	}
	
	@Test
	public void testSerialization() {
		String login = "qwertyui";
		PersonInformation information = new PersonInformation();
		information.getInformations().add(new TestsInformation("", 10, 10, 10, 10));
		set.add(new Person(login, "asd", "asd", information));
		SerializationHelper.serialize(FILENAME, set);
		ObservableSet<Person> deserializeSet = SerializationHelper.deSerialize(FILENAME, TypeOfClass.PERSON);
		assertAll("Serialization",
				() -> assertNotNull(deserializeSet),
				() -> assertEquals(login, FuncsHelper.getPerson(deserializeSet, login).getLogin()));
	}

}
